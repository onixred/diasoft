CREATE TABLE `user`(
    `id` INT(5) NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(50) NOT NULL,
    `last_name` VARCHAR(50) NOT NULL,
    `login` VARCHAR(20) NOT NULL UNIQUE,
    `password` CHAR(128) NOT NULL,
    PRIMARY KEY(`id`)
