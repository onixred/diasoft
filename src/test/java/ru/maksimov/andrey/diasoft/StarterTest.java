package ru.maksimov.andrey.diasoft;

import ru.maksimov.andrey.diasoft.Starter;

/**
 * Тестовый старт сервиса
 * 
 * @author amaksimov
 */
public class StarterTest {

	public static void main(String[] args) throws Throwable {
		Starter.main(args);
	}
}
