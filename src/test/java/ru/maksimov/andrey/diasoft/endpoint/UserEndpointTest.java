package ru.maksimov.andrey.diasoft.endpoint;

import java.io.IOException;
import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import https.bitbucket_org.onixred.diasoft.User;
import ru.maksimov.andrey.diasoft.util.Util;

import org.springframework.test.context.junit4.SpringRunner;

/**
 * Тесты для UserEndpoint
 * 
 * @author amaksimov
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserEndpointTest {

	final static String PREFIX_USER_FIRST_NAME = "first_name_";
	final static String PREFIX_USER_LAST_NAME = "last_name_";
	final static String PREFIX_USER_LOGIN = "login_";
	final static String PREFIX_USER_PASSWORD = "password_";

	final static User USER = new User();
	final static int numberUsers = 3;

	@Autowired
	private ApplicationContext applicationContext;

	private MockWebServiceClient mockClient;

	@BeforeClass
	public static void onExecutedBeforeClass() {
		int postfix = 1;
		USER.setFirstName(PREFIX_USER_FIRST_NAME + postfix);
		USER.setLastName(PREFIX_USER_LAST_NAME + postfix);
		USER.setLogin(PREFIX_USER_LOGIN + postfix);
		USER.setPassword(PREFIX_USER_PASSWORD + postfix);
	}

	@Before
	public void onExecutedBefore() {
		// TODO в идеале нужно использовать чистую БД!!!
		Assert.assertNotNull(applicationContext);
		mockClient = MockWebServiceClient.createClient(applicationContext);
		for (int i = 0; i < numberUsers; i++) {
			String request = String.format(Text.DELETE_USER_REQUEST, PREFIX_USER_LOGIN + i);
			mockClient.sendRequest(withPayload(new StringSource(request)));
		}
	}

	@After
	public void onExecutedAfter() {
		for (int i = 0; i < numberUsers; i++) {
			String request = String.format(Text.DELETE_USER_REQUEST, PREFIX_USER_LOGIN + i);
			mockClient.sendRequest(withPayload(new StringSource(request)));
		}
	}

	@Test
	public void testSaveUser() throws IOException {
		String request = String.format(Text.SAVE_USER_REQUEST, USER.getFirstName(), USER.getLastName(), USER.getLogin(),
				USER.getPassword());
		String response = String.format(Text.SAVE_USER_RESPONSE, "");

		Source responsePayload = new StringSource(response);
		mockClient.sendRequest(withPayload(new StringSource(request))).andExpect(payload(responsePayload));
	}

	@Test
	public void testDeleteUser() throws IOException {
		// add user
		addUser(USER.getFirstName(), USER.getLastName(), USER.getLogin(), USER.getPassword());

		// delete user
		String request = String.format(Text.DELETE_USER_REQUEST, USER.getLogin());
		String securePassword = Util.getSha512SecurePassword(USER.getPassword(), "impossible");
		String response = String.format(Text.DELETE_USER_RESPONSE, USER.getFirstName(), USER.getLastName(),
				USER.getLogin(), securePassword);

		Source responsePayload = new StringSource(response);
		mockClient.sendRequest(withPayload(new StringSource(request))).andExpect(payload(responsePayload));
	}

	@Test
	public void testGetUsersRequestEmpty() throws IOException {
		String request = Text.SAVE_USERS_REQUEST;
		String response = String.format(Text.SAVE_USERS_RESPONSE, "");
		Source responsePayload = new StringSource(response);
		mockClient.sendRequest(withPayload(new StringSource(request))).andExpect(payload(responsePayload));
	}

	@Test
	public void testGetUsersRequestOne() throws IOException {
		// add user
		addUser(USER.getFirstName(), USER.getLastName(), USER.getLogin(), USER.getPassword());

		String request = Text.GET_USERS_REQUEST;
		String securePassword = Util.getSha512SecurePassword(USER.getPassword(), "impossible");
		String user = String.format(Text.USERS_RESPONSE, USER.getFirstName(), USER.getLastName(), USER.getLogin(),
				securePassword);
		String response = String.format(Text.GET_USERS_RESPONSE, user);
		Source responsePayload = new StringSource(response);
		mockClient.sendRequest(withPayload(new StringSource(request))).andExpect(payload(responsePayload));
	}

	@Test
	public void testGetUsersRequestMulti() throws IOException {
		StringBuilder users = new StringBuilder();
		// add users
		for (int numberUser = 1; numberUser < numberUsers; numberUser++) {
			String firstName = PREFIX_USER_FIRST_NAME + numberUser;
			String lastName = PREFIX_USER_LAST_NAME + numberUser;
			String login = PREFIX_USER_LOGIN + numberUser;
			String password = PREFIX_USER_PASSWORD + numberUser;
			addUser(firstName, lastName, login, password);
			String securePassword = Util.getSha512SecurePassword(password, "impossible");
			String user = String.format(Text.USERS_RESPONSE, firstName, lastName, login, securePassword);
			users.append(user);
		}

		String request = Text.GET_USERS_REQUEST;
		String response = String.format(Text.GET_USERS_RESPONSE, users.toString());
		Source responsePayload = new StringSource(response);

		mockClient.sendRequest(withPayload(new StringSource(request))).andExpect(payload(responsePayload));
	}

	private void addUser(String firstName, String lastName, String login, String password) {
		String addRequest = String.format(Text.SAVE_USER_REQUEST, firstName, lastName, login, password);
		mockClient.sendRequest(withPayload(new StringSource(addRequest)));
	}
}
