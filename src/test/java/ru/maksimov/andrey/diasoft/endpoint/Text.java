package ru.maksimov.andrey.diasoft.endpoint;

public class Text {

	static final String DELETE_USER_REQUEST = "<dias:deleteUserRequest xmlns:dias='https://bitbucket.org/onixred/diasoft'>"
			+ "<dias:login>%s</dias:login>"
			+ "</dias:deleteUserRequest>";

	static final String DELETE_USER_RESPONSE ="<ns2:deleteUserResponse xmlns:ns2='https://bitbucket.org/onixred/diasoft'>"
			+ "<ns2:user>"
			+ "<ns2:first_name>%s</ns2:first_name>"
			+ "<ns2:last_name>%s</ns2:last_name>"
			+ "<ns2:login>%s</ns2:login>"
			+ "<ns2:password>%s</ns2:password>"
			+ "</ns2:user>"
			+ "</ns2:deleteUserResponse>";
	

	static final String SAVE_USER_REQUEST = "<dias:saveUserRequest xmlns:dias='https://bitbucket.org/onixred/diasoft'>"
			+ "<dias:user>" 
			+ "<dias:first_name>%s</dias:first_name>" 
			+ "<dias:last_name>%s</dias:last_name>"
			+ "<dias:login>%s</dias:login>" 
			+ "<dias:password>%s</dias:password>" 
			+ "</dias:user>"
			+ "</dias:saveUserRequest>";

	static final String SAVE_USER_RESPONSE = "<ns2:saveUserResponse xmlns:ns2='https://bitbucket.org/onixred/diasoft'>"
			+"%s"
			+ "</ns2:saveUserResponse>";

	static final String SAVE_USERS_REQUEST = "<dias:getUsersRequest xmlns:dias='https://bitbucket.org/onixred/diasoft'/>";

	static final String SAVE_USERS_RESPONSE = "<ns2:getUsersResponse xmlns:ns2='https://bitbucket.org/onixred/diasoft'>"
			+ "%s"
			+ "</ns2:getUsersResponse>";

	static final String GET_USERS_REQUEST = "<dias:getUsersRequest xmlns:dias='https://bitbucket.org/onixred/diasoft'/>";

	static final String USERS_RESPONSE = "<ns2:users>"
			+ "<ns2:first_name>%s</ns2:first_name>"
			+ "<ns2:last_name>%s</ns2:last_name>"
			+ "<ns2:login>%s</ns2:login>"
			+ "<ns2:password>%s</ns2:password>"
			+ "</ns2:users>";

	static final String GET_USERS_RESPONSE = "<ns2:getUsersResponse xmlns:ns2='https://bitbucket.org/onixred/diasoft'>" 
			+ "%s"
			+ "</ns2:getUsersResponse>";
}
