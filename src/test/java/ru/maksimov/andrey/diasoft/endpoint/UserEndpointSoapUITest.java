package ru.maksimov.andrey.diasoft.endpoint;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.TestRunner;
import com.eviware.soapui.model.testsuite.TestRunner.Status;
import com.eviware.soapui.model.testsuite.TestSuite;

import ru.maksimov.andrey.diasoft.endpoint.UserEndpointTest;

/**
 * Тесты для UserEndpoint используя SoapUI
 * 
 * Запустить сервис а затем тест
 * 
 * @author amaksimov
 */
public class UserEndpointSoapUITest {

	@Test
	@Ignore
	public void testRunner() throws Exception {
		WsdlProject project = new WsdlProject(
				UserEndpointTest.class.getResource("/users-wsdl-soapui-project.xml").getPath());
		List<TestSuite> testSuites = project.getTestSuiteList();
		for (TestSuite suite : testSuites) {
			List<com.eviware.soapui.model.testsuite.TestCase> testCases = suite.getTestCaseList();
			for (com.eviware.soapui.model.testsuite.TestCase testCase : testCases) {
				System.out.println("Running SoapUI test [" + testCase.getName() + "]");
				TestRunner runner2 = testCase.run(new PropertiesMap(), false);
				assertEquals(Status.FINISHED, runner2.getStatus());
			}
		}
	}
}
