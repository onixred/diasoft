<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Create an account</title>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<div class="container">

		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()">Logout</a>
			</h2>
			<div class="add">
				<form:form method="POST" modelAttribute="contentForm"
					class="form-add-content" action="${contextPath}/addText">
					<h2 class="form-add-content-heading">add text</h2>
					<spring:bind path="content">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<form:input type="text" path="content" class="form-control"
								placeholder="Content" autofocus="true"></form:input>
							<form:errors path="content"></form:errors>

							<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
						</div>
					</spring:bind>

					<button class="btn btn-lg btn-primary btn-block" type="submit">add
						Text</button>
				</form:form>
			</div>


		</c:if>

	</div>
	<!-- /container -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>