package ru.maksimov.andrey.diasoft.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import ru.maksimov.andrey.diasoft.persistent.User;

/**
 * Вспомогательный класс
 * 
 * @author amaksimov
 */
public class Util {

	/**
	 * Получить Hash из строки
	 * 
	 * @param str
	 *            строка
	 * @param salt
	 *            секретная строка
	 * @return hash строка
	 */
	public static String getSha512SecurePassword(String str, String salt) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(salt.getBytes(StandardCharsets.UTF_8));
			byte[] bytes = md.digest(str.getBytes(StandardCharsets.UTF_8));
			StringBuilder sb = new StringBuilder();
			for (byte aByte : bytes) {
				sb.append(Integer.toString((aByte & 0xff) + 0x100, 16)
						.substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	/**
	 * Конвертация из persistent в DTO объекта
	 * 
	 * @param persistentUser
	 *            persistent "Пользователь"
	 * @return DTO пользователь
	 */
	public static https.bitbucket_org.onixred.diasoft.User convertUser2UserDto(
			User persistentUser) {
		https.bitbucket_org.onixred.diasoft.User user = new https.bitbucket_org.onixred.diasoft.User();
		user.setFirstName(persistentUser.getFirstName());
		user.setLastName(persistentUser.getLastName());
		user.setLogin(persistentUser.getLogin());
		user.setPassword(persistentUser.getPassword());
		return user;
	}

	public static String validate(
			https.bitbucket_org.onixred.diasoft.User user) {
		StringBuilder errorMessage = new StringBuilder();
		if (user == null) {
			errorMessage.append("Error user is null.");
		} else {
			if (org.apache.commons.lang.StringUtils
					.isBlank(user.getFirstName())) {
				errorMessage.append("Error FirstName is blank.\n");
			}
			if (org.apache.commons.lang.StringUtils
					.isBlank(user.getLastName())) {
				errorMessage.append("Error Last Name is blank.\n");
			}
			if (org.apache.commons.lang.StringUtils.isBlank(user.getLogin())) {
				errorMessage.append("Error login is blank.\n");
			}
			if (org.apache.commons.lang.StringUtils
					.isBlank(user.getPassword())) {
				errorMessage.append("Error password is blank.\n");
			}
		}
		return errorMessage.toString();
	}
}
