package ru.maksimov.andrey.diasoft.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.maksimov.andrey.diasoft.model.Text;
import ru.maksimov.andrey.diasoft.model.User;
import ru.maksimov.andrey.diasoft.service.SecurityService;
import ru.maksimov.andrey.diasoft.service.UserService;

/**
 * Контроллер
 * 
 * @author amaksimov
 */
@Controller
public class UserController {

	private static final Logger LOG = LogManager
			.getLogger(UserController.class);

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registration(@ModelAttribute("userForm") User userForm,
			BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			return "registration";
		}

		userService.save(userForm);

		securityService.autologin(userForm.getUsername(),
				userForm.getPasswordConfirm());
		return "redirect:/welcome";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error",
					"Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message",
					"You have been logged out successfully.");

		return "login";
	}

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String welcome(Model model) {
		model.addAttribute("contentForm", new Text());
		return "welcome";
	}

	@RequestMapping(value = "/addText", method = RequestMethod.POST)
	public String welcome(@ModelAttribute("contentForm") Text contentForm,
			BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "registration";
		}
		// todo save text!!!
		LOG.info("Saved text " + contentForm.getContent());
		return "redirect:/welcome";
	}

}
