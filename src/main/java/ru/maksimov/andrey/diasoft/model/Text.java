package ru.maksimov.andrey.diasoft.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Сущностью "Контекст" (используем JPA Entities)
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "content_jpa")
public class Text {

	private Long id;
	private String content;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
