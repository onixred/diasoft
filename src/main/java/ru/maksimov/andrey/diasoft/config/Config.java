package ru.maksimov.andrey.diasoft.config;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;

/**
 * Конфиг
 * 
 * @author amaksimov
 */
public class Config {

	private static final Config INSTANCE = new Config();

	private ObjectContext objectContext;

	

	private Config() {
		this.objectContext = ServerRuntime.builder().addConfig("cayenne-diasoft.xml").build().newContext();
	};

	/**
	 * @return экземпляр конфига
	 */
	public static Config getConfig() {
		return INSTANCE;
	}

	/**
	 * @return экземпляр objectContext
	 */
	public ObjectContext getObjectContext() {
		return objectContext;
	}
}
