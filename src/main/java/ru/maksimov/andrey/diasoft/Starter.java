package ru.maksimov.andrey.diasoft;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Прототип "задача для diasoft"
 * 
 * @author amaksimov
 */
@SpringBootApplication()
@ComponentScan(basePackages = {"ru.maksimov.andrey.commons", "ru.maksimov.andrey.diasoft" })
public class Starter {

	private static final Logger LOG = LogManager.getLogger(Starter.class);

	public static void main(String[] args) {
		SpringApplication.run(Starter.class, args);
		LOG.info("APPLICATION STARTED");
	}
}
