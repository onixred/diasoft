package ru.maksimov.andrey.diasoft.endpoint;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import https.bitbucket_org.onixred.diasoft.DeleteUserRequest;
import https.bitbucket_org.onixred.diasoft.DeleteUserResponse;
import https.bitbucket_org.onixred.diasoft.GetUsersRequest;
import https.bitbucket_org.onixred.diasoft.GetUsersResponse;
import https.bitbucket_org.onixred.diasoft.ObjectFactory;
import https.bitbucket_org.onixred.diasoft.SaveUserRequest;
import https.bitbucket_org.onixred.diasoft.SaveUserResponse;
import https.bitbucket_org.onixred.diasoft.User;
import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.diasoft.service.UserService;

/**
 * Endpoint для User
 * 
 * @author amaksimov
 */
@Endpoint
public class UserEndpoint {

	public static final String NAMESPACE_URI = "https://bitbucket.org/onixred/diasoft";

	private static final Logger LOG = LogManager.getLogger(UserEndpoint.class);

	@Autowired
	UserService userService;

	//TODO Перейти на Log4j 2 и использовать аннотацию @Loggable (ru.maksimov.andrey.commons.log), или добавить поддержку Log4j 1.x
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUsersRequest")
	@ResponsePayload
	public GetUsersResponse getUsers(@RequestPayload GetUsersRequest request) {
		LOG.info("Start execution 'getUsers()' ");
		ObjectFactory of = new ObjectFactory();
		GetUsersResponse response = of.createGetUsersResponse();
		response.getUsers();
		try {
			List<User> users = userService.findAll();
			if (!users.isEmpty()) {
				response.getUsers().addAll(users);
			}
		} catch (BusinessException se) { 
			https.bitbucket_org.onixred.diasoft.Error error = of.createError();
			response.setError(error);
			error.setCode(500);
			error.setMessage("Unable get users. Message error: " + se.getMessage());
		}
		LOG.info("Start Finished 'getUsers()' result: " + response.toString());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveUserRequest")
	@ResponsePayload
	public SaveUserResponse saveUser(@RequestPayload SaveUserRequest request) {
		LOG.info("Start execution 'saveUser()' ");
		ObjectFactory of = new ObjectFactory();
		SaveUserResponse response = of.createSaveUserResponse();
		
		try {
			userService.save(request.getUser());
		} catch (VerificationException ve) {
			https.bitbucket_org.onixred.diasoft.Error error = of.createError();
			response.setError(error);
			error.setCode(400);
			error.setMessage("Unable to save user. Message error: " + ve.getMessage());
		} catch (BusinessException se) { 
			https.bitbucket_org.onixred.diasoft.Error error = of.createError();
			response.setError(error);
			error.setCode(500);
			error.setMessage("Unable to save user. Message error: " + se.getMessage());
		}
		LOG.info("Start Finished 'getUsers()' result: " + response.toString());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteUserRequest")
	@ResponsePayload
	public DeleteUserResponse deleteUser(@RequestPayload DeleteUserRequest request) {
		LOG.info("Start execution 'deleteUser(DeleteUserRequest " + request.toString()  + ")'");
		ObjectFactory of = new ObjectFactory();
		DeleteUserResponse response = of.createDeleteUserResponse();
		try {
			response.setUser(userService.delete(request.getLogin()));
		} catch (VerificationException ve) {
			https.bitbucket_org.onixred.diasoft.Error error = of.createError();
			response.setError(error);
			error.setCode(400);
			error.setMessage("Unable to delete user. Message error: " + ve.getMessage());
			
		} catch (BusinessException se) { 
			https.bitbucket_org.onixred.diasoft.Error error = of.createError();
			response.setError(error);
			error.setCode(500);
			error.setMessage("Unable to delete user. Message error: " + se.getMessage());
		}
		LOG.info("Start Finished 'deleteUser()' result: " + response.toString());
		return response;
	}
}
