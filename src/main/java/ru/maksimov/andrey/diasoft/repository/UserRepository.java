package ru.maksimov.andrey.diasoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.diasoft.model.User;

/**
 * Интерфейс по работе с хранилищем пользователя
 * 
 * @author amaksimov
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
