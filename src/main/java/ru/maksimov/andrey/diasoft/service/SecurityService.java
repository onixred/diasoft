package ru.maksimov.andrey.diasoft.service;

/**
 * Интерфейс сервиса по доступом
 * 
 * @author amaksimov
 */
public interface SecurityService {

	/**
	 * Найти имя пользователя авторизации
	 * 
	 * @return имя пользователя
	 */
	String findLoggedInUsername();

	/**
	 * Автоматический вход
	 * 
	 * @param username
	 *            имя пользователя
	 * @param password
	 *            пароль
	 */
	void autologin(String username, String password);

}
