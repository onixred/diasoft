package ru.maksimov.andrey.diasoft.service;

import java.util.List;

import https.bitbucket_org.onixred.diasoft.User;
import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "Пользователь"
 * 
 * @author amaksimov
 */
public interface UserService {

	/**
	 * Найти всех пользователей
	 * 
	 * @return список пользователей
	 */
	List<User> findAll() throws BusinessException;

	/**
	 * Сохранить/обновить пользователя
	 * 
	 * @param user
	 *            пользователь
	 */
	void save(User user) throws VerificationException, BusinessException;

	/**
	 * Удалить пользователя
	 * 
	 * @param login
	 *            логин пользователя
	 * @return пользователь
	 */
	User delete(String login) throws VerificationException, BusinessException;

	/**
	 * Найти пользователя по имени
	 * 
	 * @param name
	 *            имя пользователя
	 * @return пользователь
	 */
	public ru.maksimov.andrey.diasoft.model.User findByUsername(String name);

	/**
	 * Сохранить/обновить пользователя
	 * 
	 * @param userForm
	 *  пользователь
	 */
	void save(ru.maksimov.andrey.diasoft.model.User userForm);
}
