package ru.maksimov.andrey.diasoft.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.exp.Expression;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import https.bitbucket_org.onixred.diasoft.User;
import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.diasoft.config.Config;
import ru.maksimov.andrey.diasoft.repository.UserRepository;
import ru.maksimov.andrey.diasoft.service.UserService;
import ru.maksimov.andrey.diasoft.util.Util;

/**
 * Сервис по работе с сущностью "Пользователь"
 * 
 * @author amaksimov
 */
@Service
public class UserServiceImpl implements UserService {

	private static String SALT = "impossible";

	private static Config config = Config.getConfig();

	private static final Logger LOG = LogManager
			.getLogger(UserServiceImpl.class);

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> findAll() {
		ObjectContext context = config.getObjectContext();
		SelectQuery<ru.maksimov.andrey.diasoft.persistent.User> query = new SelectQuery<ru.maksimov.andrey.diasoft.persistent.User>(
				ru.maksimov.andrey.diasoft.persistent.User.class);
		List<ru.maksimov.andrey.diasoft.persistent.User> pUsers = context
				.select(query);
		LOG.debug("Find all users: " + pUsers.stream().map(Object::toString)
				.collect(Collectors.joining(",")));
		List<User> user = pUsers.stream()
				.map(pUser -> Util.convertUser2UserDto(pUser))
				.collect(Collectors.toList());
		return user;
	}

	@Override
	public void save(User user)
			throws VerificationException, BusinessException {
		validate(user);
		ObjectContext context = config.getObjectContext();
		ru.maksimov.andrey.diasoft.persistent.User persistent = context
				.newObject(ru.maksimov.andrey.diasoft.persistent.User.class);
		persistent.setFirstName(user.getFirstName());
		persistent.setLastName(user.getLastName());
		persistent.setLogin(user.getLogin());
		String securePassword = Util.getSha512SecurePassword(user.getPassword(),
				SALT);
		LOG.debug("Get secure password: " + securePassword);
		persistent.setPassword(securePassword);
		try {
			context.commitChanges();
		} catch (Exception e) {
			throw new BusinessException("Unable commit change. Message: "
					+ e.getCause().getMessage(), e);
		} finally {
			context.rollbackChanges();
		}
	}

	@Override
	public User delete(String login)
			throws VerificationException, BusinessException {
		validate(login, "Error login is blank.\n");
		// TODO реализовать поиск не только по логину
		ObjectContext context = config.getObjectContext();
		Expression qualifier = ExpressionFactory.matchExp(
				ru.maksimov.andrey.diasoft.persistent.User.LOGIN.getName(),
				login);
		SelectQuery<ru.maksimov.andrey.diasoft.persistent.User> select = new SelectQuery<ru.maksimov.andrey.diasoft.persistent.User>(
				ru.maksimov.andrey.diasoft.persistent.User.class, qualifier);
		ru.maksimov.andrey.diasoft.persistent.User persistent = null;
		try {
			persistent = context.selectOne(select);
		} catch (Exception e) {
			throw new BusinessException("Unable  to find user for Login: "
					+ login + ". Message: " + e.getMessage(), e);
		}
		if (persistent != null) {
			context.deleteObject(persistent);
			context.commitChanges();
		} else {
			throw new BusinessException(
					"Unable to find user for Login: " + login);
		}
		return Util.convertUser2UserDto(persistent);
	}

	/**
	 * Удалить пользователя
	 * 
	 * @param user
	 *            пользователь
	 */
	private void validate(User user) throws VerificationException {
		String errorMsg = Util.validate(user);
		if (!errorMsg.isEmpty()) {
			throw new VerificationException(errorMsg);
		}
	}

	/**
	 * Валидация строки на пуситоту
	 * 
	 * @param field
	 *            строка
	 * @param message
	 *            сообщение об ошибке
	 */
	private void validate(String field, String message)
			throws VerificationException {
		if (StringUtils.isBlank(field)) {
			throw new VerificationException(message);
		}
	}

	/**
	 * @return сущность Пользователь
	 */
	public ru.maksimov.andrey.diasoft.model.User findByUsername(String name) {
		return userRepository.findByUsername(name);
	}

	@Override
	public void save(ru.maksimov.andrey.diasoft.model.User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}

}
